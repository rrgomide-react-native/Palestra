import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  Image,
  ScrollView,
  Dimensions
} from 'react-native';

import { images } from './src/files/images';

const largura = Dimensions.get('screen').width;
const altura = largura;

export default class Palestra extends Component {

  constructor() {
    super();
    this.state = {
      dados: []
    }
  }

  componentDidMount() {

    //https://api.myjson.com/bins/13f28h
    fetch('https://api.myjson.com/bins/13f28h')
      .then(resposta => resposta.json())
      .then(json => this.setState({dados: json}));
  }

  render() {

    return (
      <ScrollView style={{margin: 5}} >
        {
          this.state.dados.map(item =>
            <View
              key={item.id}
            >
              <Image
                style={{width: largura, height: altura}}
                source={{uri: item.uri}}
              />
              <Text style={{fontWeight: 'bold'}} >
                {item.usuario}
              </Text>
              <Text style={{marginBottom: 20, borderBottomWidth: 1, borderBottomColor: '#ddd'}}>
                {item.curtidas} curtida(s)
              </Text>
            </View>
          )
        }
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});

AppRegistry.registerComponent('Palestra', () => Palestra);
